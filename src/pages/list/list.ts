import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';

import { ModalController, LoadingController } from 'ionic-angular';
import { GalleryModal } from 'ionic-gallery-modal';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';

declare var cordova: any;

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage implements OnInit {
  list = [];
  storageDirectory: string = '';
  fileTransfer: FileTransferObject = this.transfer.create();
  loading;

  constructor(public navCtrl: NavController, public navParams: NavParams, private modalCtrl: ModalController, private transfer: FileTransfer, private file: File, public platform: Platform, public loadingCtrl: LoadingController) {
    
    this.platform.ready().then(() => {
      if (this.platform.is('ios')) {
        this.storageDirectory = cordova.file.documentsDirectory;
      }
      else if(this.platform.is('android')) {
        this.storageDirectory = cordova.file.dataDirectory;
      }      
      else {
        // exit otherwise, but you could add further types here e.g. Windows
        return false;
      }
    })

    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    this.loading.present();
  }

  getFileURI(uri) {
    if (this.platform.is('ios')) {
      return uri.replace(/^file:\/\//, '');
    }
    return uri;
  }

  downloadImage(tag, i, size, type) {
    return new Promise((resolve, reject) => {
      const url = `http://lorempixel.com/${size.width}/${size.height}/${tag}/${i}`;
      this.fileTransfer.download(url, this.file.dataDirectory + `${tag}/${i}`).then((entry) => {
        localStorage.setItem(`${tag}/${i}/${type}`, this.getFileURI(entry.toURL()))
        console.log('download complete: ' + this.getFileURI(entry.toURL()));
        resolve()
      }, (error) => {
        // handle error
        console.log(error.toString());
        resolve()
      });
    })
  }

  ngOnInit() {
    this.platform.ready().then(() => {

      const tags = ['sports', 'animals', 'food', 'nature', 'cats'];
      let images = []
      let promises = []
      
      tags.forEach(tag => {
        for (var i = 1; i < 11; i++) {
          promises.push(this.downloadImage(tag, i, { width: 400, height: 200 }, 'thumbnail'));
          promises.push(this.downloadImage(tag, i, { width: 1920, height: 1920 }, 'full'));
          images.push(`${tag}/${i}`)
        }
      })
      localStorage.setItem('images', JSON.stringify(images));

      Promise.all(promises)
        .then(() => {
          const list = JSON.parse(localStorage.getItem('images'));
          list.forEach(item => {
            this.list.push({
              thumbnail: localStorage.getItem(`${item}/thumbnail`),
              fullSize: localStorage.getItem(`${item}/full`)
            });
          });
          this.loading.dismiss();
        })
        .catch(error => {
          console.log(error.toString());
          this.loading.dismiss();
        })
      
    });
  }

  itemTapped(event, item) {
    let modal = this.modalCtrl.create(GalleryModal, {
      photos: [{
        url: item.thumbnail
      }],
      initialSlide: 0
    });
    modal.present();
  }
}
